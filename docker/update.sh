git clean -df
git reset --hard
git pull
cp .env.production .env
chown -R ubuntu:www-data /home/ubuntu/docker/green_trade_docker
chmod -R 777 /home/ubuntu/docker/green_trade_docker
docker compose up -d
