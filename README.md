**台灣全島用電負載預測**

**DEMO網址**

還沒建好

| 技術領域         | 工具/框架                |
| ---------------- | ----------------------- |
| 後端             | PHP 8, Laravel 10       |
| 前端             | Vue 3, Inertia          |
| 資料庫           | PostgreSQL              |
| 機器學習建模     | Python, Keras, RNN      |
| Web 伺服器       | Nginx                   |
| 環境建置         | Docker                  |

**教學文件**

https://hackmd.io/7_elpcd0QjKyh2p0qu2-rA